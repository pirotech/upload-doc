import React from 'react'
import {Uploader} from './Uploader'
import styles from './App.module.scss'

function App() {

  return (
   <div className={styles.wrapper}>
    <Uploader />
   </div>
  )
}

export default App
