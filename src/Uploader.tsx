import React, {useCallback, useEffect, useState} from 'react'
import {useDropzone} from 'react-dropzone'
import classNames from 'classnames'
import styles from './Uploader.module.scss'

export const Uploader: React.FC = () => {
  const [prediction, setPrediction] = useState(0)
  const [loading, setLoading] = useState(false)

  const onDrop = useCallback(async (acceptedFile: File[]) => {
    const formData = new FormData()
    formData.append('file', acceptedFile[0])
    setLoading(true)
    const result = await fetch(import.meta.env.VITE_BASE_URL, {
      method: "POST",
      body: formData,
    })
    const json = (await result.json()) as {prediction: string}
    setPrediction(Math.round(parseFloat(json.prediction) * 100));
    setLoading(false)
  }, [])

  const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})

  return (
    <div className={styles.aaa}>
      <div className={classNames(styles.wrapper, loading && styles.wrapper_pulse)}>
        <div className={styles.inputWrapper} {...getRootProps()}>
          <input {...getInputProps()} />
          {
            isDragActive ?
              <p>Перетащи файл сюда ...</p> :
              <p>Перетащи файл или кликни</p>
          }
        </div>
        <div className={styles.progress}>
          <div className={styles.progress__inner} style={{ width: `${prediction}%`}} />
        </div>
      </div>
      {!loading && <div className={styles.prediction}>
        похоже на {prediction}%
      </div>}
    </div>
  )
}